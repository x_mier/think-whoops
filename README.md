使用方式：
// Whoops 接管请求异常
    $whoops = new \Whoops\Run;
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
    return Response::create(
        $whoops->handleException($e),
        'html',
        500
    );